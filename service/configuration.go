package service

import (
	"encoding/json"
	"io/ioutil"
	"strings"
)

// configuration
type Configuration struct {
	SecurityManager struct {
		Hostname string `json:"hostname"`

		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"securityManager"`

	ResourceFolder string `json:"resourceFolder"`

	// [ type ] map[ name ] iconName
	Resource map[string]map[string]string `json:"resource"`
}

// build configuration
func BuildConfiguration(configurationFilePath string) (*Configuration, error) {
	// configuration
	configuration := &Configuration{}

	// load file
	if fileContent, err := ioutil.ReadFile(configurationFilePath); err == nil {
		// unmarshal
		if err := json.Unmarshal(fileContent,
			configuration); err == nil {
			configuration.ResourceFolder = strings.TrimSuffix(configuration.ResourceFolder,
				"/") +
				"/"
			return configuration, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}
