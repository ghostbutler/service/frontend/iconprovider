package service

import (
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"io/ioutil"
	"net/http"
	"strings"
)

// define your services ids, must start from last built in index
const (
	// get icon
	APIServiceV1IconProviderGet = common.APIServiceType(iota + common.APIServiceBuiltInLast)
)

// declare how you want your endpoints to be accessed to
// Path is the split version of URL.Path by '/' character. So to express /api/v1/first, you'll put here
// ["api", "v1", "first"]
// Method is the HTTP method to use to access this endpoint
// Callback is a function of type common.EndpointFunction which will be called when an HTTP request triggers an endpoint
var DirectoryAPIService = map[common.APIServiceType]*common.APIEndpoint{
	// directory services
	APIServiceV1IconProviderGet: {
		Path:                    []string{"api", "v1", "iconprovider"},
		Method:                  "GET",
		Description:             "get icon",
		Callback:                CallbackAPIServiceV1IconProviderGet,
		IsMustProvideOneTimeKey: false,
	},
}

// get an icon (type+name)
func CallbackAPIServiceV1IconProviderGet(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handler interface{}) int {
	srv := handler.(*Service)
	if err := request.ParseForm(); err == nil {
		_type := request.FormValue("type")
		name := request.FormValue("name")

		if typeMap, ok := srv.configuration.Resource[_type]; ok {
			fileName := ""
			if nameResult, ok := typeMap[strings.ToLower(name)]; ok {
				fileName = nameResult
			} else {
				fileName = typeMap["default"]
			}

			if fileContent, err := ioutil.ReadFile(srv.configuration.ResourceFolder +
				fileName); err == nil {
				rw.Header().Add("Content-Type",
					"image/png")
				_, _ = rw.Write(fileContent)
			} else {
				rw.WriteHeader(http.StatusInternalServerError)
				return http.StatusInternalServerError
			}

			fmt.Println("Now serving",
				fileName)

			return http.StatusOK
		} else {
			rw.WriteHeader(http.StatusBadRequest)
			return http.StatusBadRequest
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}
}
