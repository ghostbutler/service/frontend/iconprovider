package service

// we do use this library to declare our endpoints
import (
	"crypto/tls"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"net/http"
	"time"
)

// set here your service version
const (
	VersionMajor = 1
	VersionMinor = 0
)

// this is your service structure, which can contains more then the simple common.Service instance
type Service struct {
	// service
	service *common.Service

	// configuration
	configuration *Configuration

	// key manager
	keyManager *keystore.KeyManager

	// http client
	httpClient *http.Client

	// is running?
	isRunning bool
}

// this is the function you will use to build your service
func BuildService(listeningPort int,
	configuration *Configuration) (*Service, error) {
	service := &Service{
		configuration: configuration,
		isRunning:     true,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceDirectory].Name),
		httpClient: &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			},
		},
	}

	// build service with previous specified data
	service.service = common.BuildService(listeningPort,
		common.ServiceDirectory,
		DirectoryAPIService,
		VersionMajor,
		VersionMinor,
		nil,
		nil,
		configuration.SecurityManager.Hostname,
		service)

	// run updater
	go service.updateThread()

	// service is built
	return service, nil
}

// close
func (service *Service) Close() {
	service.isRunning = false
}

// update thread
func (service *Service) updateThread() {
	for service.isRunning {

		// sleep
		time.Sleep(time.Millisecond * 16)
	}
}

// is running?
func (service *Service) IsRunning() bool {
	return service.isRunning
}
